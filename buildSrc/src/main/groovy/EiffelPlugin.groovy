import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.GradleException

class EiffelPlugin implements Plugin<Project> {

    private final EIFFEL_ENV_VAR = 'ISE_EIFFEL'
    private final PLATFORM_ENV_VAR = 'ISE_PLATFORM'
    private final ISE_COMPILER = 'ec'
    private final ISE_FINALIZER = 'finish_freezing'

    @Override
    void apply(Project project) {
        // Add the 'eiffel' extension object
        project.extensions.create('eiffel', EiffelPluginExtension)

        // check env vars
        def baseDir = System.getenv(EIFFEL_ENV_VAR)
        def platform = System.getenv(PLATFORM_ENV_VAR)
        if (baseDir == null || platform == null)
            throw new GradleException('Missing env vars: ' + EIFFEL_ENV_VAR + ' and/or ' + PLATFORM_ENV_VAR)
        def binsPath = "${baseDir}/studio/spec/${platform}/bin/"
        def cfg = project.file("src/${project.eiffel.targetcfg}")

        project.task('meltEiffel') {
            doFirst {
                // create build folder
                if (!project.getBuildDir().exists())
                    project.getBuildDir().mkdir()
            }

            doLast {
                project.exec {
                    workingDir project.getBuildDir()
                    def cmd = commandLine binsPath + ISE_COMPILER, '-batch', '-melt', '-config', cfg
                    if (cmd.execute().getExitValue() != 0)
                        throw new GradleException('Melting failed!')
                }
            }
        }

        project.task('freezeEiffel') {
            dependsOn('meltEiffel')
            doLast {
                project.exec {
                    workingDir project.getBuildDir()
                    def cmd = commandLine binsPath + ISE_COMPILER, '-batch', '-freeze', '-config', cfg
                    if (cmd.execute().getExitValue() != 0)
                        throw new GradleException('Freezing failed!')
                }
            }
        }

        project.task('finalizeEiffel') {
            dependsOn('freezeEiffel')
            doLast {
                project.exec {
                    workingDir project.file(project.getBuildDir().toString() + "/EIFGENs/${project.eiffel.target}/W_code")
                    def cmd = commandLine binsPath + ISE_FINALIZER, '-silent'
                    if (cmd.execute().getExitValue() != 0)
                        throw new GradleException('Freeze finalizing failed!')
                }
            }
        }

        project.task('clean') {
            doLast {
                project.getBuildDir().deleteDir()
            }
        }
    }
}

class EiffelPluginExtension {
    def String target = 'hello'
    def String targetcfg = 'hello.ecf'
}
