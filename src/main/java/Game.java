public class Game {

	private int rolls[];
	private int currentRoll;

	public Game() {
		this.rolls = new int[21];
		this.currentRoll = 0;
	}

	public void roll(int pins) {
		rolls[currentRoll++] = pins;
	}

	public int score() {
		int score = 0;
		int i = 0;
		for (int frame = 0; frame < 10; frame++) {
			if (isStrike(i)) {
				score += 10 + strikeBonus(i);
				i++;
			} else if (isSpare(i)) {
				score += 10 + spareBonus(i);
				i += 2;
			} else {
				score += scoreNormalFrame(i);
				i += 2;
			}
		}
		return score;
	}

	private boolean isSpare(int frame) {
		return rolls[frame] + rolls[frame + 1] == 10;
	}

	private boolean isStrike(int frame) {
		return rolls[frame] == 10;
	}

	private int strikeBonus(int frame) {
		return rolls[frame + 1] + rolls[frame + 2];
	}

	private int spareBonus(int frame) {
		return rolls[frame + 2];
	}

	private int scoreNormalFrame(int frame) {
		return rolls[frame] + rolls[frame + 1];
	}
}
