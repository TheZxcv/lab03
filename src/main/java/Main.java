import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) {
		Game game = new Game();

		for (int i = 0; i < 10; i++) {
			game.roll(10);
			logger.debug("Current score: {}", game.score());
		}
		game.roll(0);
		logger.debug("Final score: {}", game.score());
		System.out.println(game.score());
	}
}
